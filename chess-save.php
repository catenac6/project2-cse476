<?php
require_once "db.inc.php";
echo '<?xml version="1.0" encoding="UTF-8" ?>';

// Ensure the userid post item exists
if(!isset($_POST['user'])) {
    echo '<chess status="no" msg="missing user" />';
    exit;
}
// Ensure the magic post item exists
if(!isset($_POST['magic'])) {
    echo '<chess status="no" msg="missing magic" />';
    exit;
}
// Ensure the password post item exists
if(!isset($_POST['pw'])) {
    echo '<chess status="no" msg="missing password" />';
    exit;
}
// Ensure the xml post item exists
if(!isset($_POST['xml'])) {
    echo '<chess status="no" msg="missing XML" />';
    exit;
}

if(!isset($_POST['magic']) || $_POST['magic'] != "NechAtHa6RuzeR8x") {
    echo '<chess status="no" msg="magic" /&>';
    exit;
}
$user = $_POST["user"];
$password = $_POST["pw"];
$pdo = pdo_connect();
$userid = getUser($pdo, $user, $password);

processXml($pdo, $userid, stripslashes($_POST['xml']));

echo '<chess status="yes"/>';

function getUser($pdo, $user, $password) {
    // Does the user exist in the database?
    $userQ = $pdo->quote($user);
    $query = "SELECT id, password from chessuser where user=$userQ";
    $rows = $pdo->query($query);
    if($row = $rows->fetch()) {
        // We found the record in the database
        // Check the password
        if($row['password'] != $password) {
            echo '<chess status="no" msg="password error" />';
            exit;
        }

        return $row['id'];
    }

    echo '<chess status="no" msg="user error" />';
    exit;
}
/**
 * Process the XML query
 * @param $xmltext the provided XML
 */
function processXml($pdo, $userid, $xmltext)
{
    // Load the XML
    $xml = new XMLReader();
    if (!$xml->XML($xmltext)) {
        echo '<chess status="no" msg="invalid XML" />';
        exit;
    }
    // Read to the chess tag
    while ($xml->read()) {
        if ($xml->nodeType == XMLReader::ELEMENT &&
            $xml->name == "chess") {

            $name = $xml->getAttribute("name");
            $x = $xml->getAttribute("x");
            $y = $xml->getAttribute("y");

            $nameQ = $pdo->quote($name);

            // Checks
            if (!is_numeric($x) || !is_numeric($y)) {
                echo '<chess status="no" msg="invalid" />';
                exit;
            }
            $query = <<<QUERY
REPLACE INTO chess(name, userid, x, y)
VALUES($nameQ, '$userid', $x, $y)
QUERY;
            if(!$pdo->query($query)) {
                echo '<chess status="no" msg="insertfail">' . $query . '</chess>';
                exit;
            }

            echo '<chess status="yes"/>';
            exit;
        }
        echo '<chess save="no" msg="invalid XML" />';
    }
    exit;
}
