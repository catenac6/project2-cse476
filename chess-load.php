<?php
/*
 * Chess application loading
 */
require_once "db.inc.php";
echo '<?xml version="1.0" encoding="UTF-8" ?>';

if(!isset($_GET['magic']) || $_GET['magic'] != "NechAtHa6RuzeR8x") {
    echo '<chess status="no" msg="magic" />';
    exit;
}

if(!isset($_GET['user'])) {
    echo '<chess status="no" msg="username" />';
    exit;
}

if(!isset($_GET['pw'])) {
    echo '<chess status="no" msg="password" />';
    exit;
}

if(!isset($_GET['id'])) {
    echo '<chess status="no" msg="id" />';
    exit;
}

// Process in a function
process($_GET['user'], $_GET['pw'], $_GET['id']);

/**
 * Process the query
 * @param $user the user to look for
 * @param $password the user password
 */
function process($user, $password, $idQ) {
    // Connect to the database
    $pdo = pdo_connect();

    $userid = getUser($pdo, $user, $password);
    $query = "select name, x, y from chess where id=$idQ";
    $rows = $pdo->query($query);

    echo "<chess status=\"yes\">\r\n";
    foreach($rows as $row ) {
        $name = $row['name'];
        $x = $row['x'];
        $y = $row['y'];
        
        echo "<name id=\"$idQ\" name=\"$name\" x=\"$x\" y=\"$y\" />\r\n";
    }

    echo "</chess>";
}

/**
 * Ask the database for the user ID. If the user exists, the password
 * must match.
 * @param $pdo PHP Data Object
 * @param $user The user name
 * @param $password Password
 * @return id if successful or exits if not
 */
function getUser($pdo, $user, $password) {
    // Does the user exist in the database?
    $userQ = $pdo->quote($user);
    $query = "SELECT id, password from chessuser where user=$userQ";

    $rows = $pdo->query($query);
    if($row = $rows->fetch()) {
        // We found the record in the database
        // Check the password
        if($row['password'] != $password) {
            echo '<chess status="no" msg="password error" />';
            exit;
        }

        return $row['id'];
    }

    echo '<chess status="no" msg="user error" />';
    exit;
}
